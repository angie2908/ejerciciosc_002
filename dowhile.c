#include <stdio.h>
 
int main(){
   
    int numero;
    int sumaTotal=0;
 
    do{
        printf("Dame un numero\n");
        scanf("%d", &numero);
        getchar();
        sumaTotal = sumaTotal + numero; // sumaTotal+=numero;
    }while(numero!=0);
   
    printf("Resultado = %d\n",sumaTotal);
   
    getchar();
   
    return 0;
}